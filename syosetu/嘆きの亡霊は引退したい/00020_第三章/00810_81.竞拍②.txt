「真是見鬼。轉了六處卻連一個都沒有發現！一個都沒有哦？」

「嗯嗯。畢竟這個時期人很多呢」

時隔幾日歸來的莉茲熱切地向我講述。
結果姑且不論，她看來是安然無恙。就算她的資質高於認證等級，接連探索六處寶物殿也是聞所未聞，可她的表情卻看不出疲憊。

「猶豫過是去遠征還是去尋找賞金犯。可是尋找賞金犯和換錢都要花費時間，遠征也要移動時間，在這期間競拍也許就開始了對吧？因此，與其趕不上競拍，不如把打倒的幻影和魔物的掉落物賣掉，盡可能地增加資金會更好吧──我是這麼想的。給。這是變賣換來的錢！」

莉茲面帶笑容，把一個大皮袋硬塞似地遞給我。
對獵人來說，出售魔物的素材和幻影的掉落物所獲得的收入不可小覷。
與看運氣獲得的寶具和幻影的掉落物不同，魔物的素材是只要打倒魔物就必定得到，因此，在擁有一定的高超戰鬥技巧的獵人中，也有人把魔物鎖定為目標。

莉茲遞來的袋子裡裝滿了金幣。雖然她很淡然，但不努力的話就不可能達到這個數目。
窺視了一下袋子後，西特莉皺起眉說道。

「姐姐，真沒用」

「⋯⋯哈～？事情變成這樣都是你的錯吧！為什麼克萊伊盯上寶具的事傳開啦！」

抱歉，那是我的錯⋯⋯⋯

看著互相戲弄的姐妹，罪惡感便涌現出來，我無言地移開了視線。

進入拍賣會舉辦期間，帝都人流如潮。
澤布魯迪亞拍賣會將要開展一周。在此期間，帝都將會熱鬧無比。
主要街道擺滿了攤位，假借正牌拍賣會的名義，到處都有小型拍賣會在舉辦。探索者協會擠滿了來接委託的人，對商人和獵人來說，這都是賺錢的時候。

近段時間，我考慮的盡是『轉換的人面』，可是冷靜一想，拍賣會也會展出其他有用的寶具吧。然而現在的經濟狀況不允許我向那邊出手，像是無法參加難得的祭典一樣，我有些空虛。

被莉茲和西特莉夾在中間，我行走在人群中。雖然也有扒手頻繁出沒，但只要有莉茲在就不用擔心被盜。

「唔～嗯。就算加上姐姐那份，也不到十億呢⋯⋯因為你不存錢」

「沒有辦法吧？我傾注了全力去攻略【城】，並且戰果在盧克他們的手上⋯⋯」

【萬魔之城】是長期以來，沒有出現過任何攻略者的寶物殿。那裡應該沉睡著值得冒險的寶藏吧。
我們的隊伍有一條規則，任何成員如果中途離隊，就不帶回戰果。
假如沒有那條規則，莉茲或西特莉哪怕只帶回一點，這次競拍的結果都會改變也說不定。

看著撅起嘴的莉茲，西特莉輕輕嘆了一口氣。

「沒錯呢⋯⋯時機真差。平時的話，事情的進展會更順利一些，但⋯⋯」

採取了那麼多能採取的對策，還是不夠嗎⋯⋯⋯

西特莉視線朝上看向我。

「現階段的勝率大概只有七成吧。如果克萊伊桑不說NO的話，還有更多能採取的手段，但⋯⋯」

「NO。西特莉已經做得足夠多啦。謝謝呢」

「哪有⋯⋯」

聽到我的話，西特莉露出了嬌羞的笑容。
雖然西特莉比莉茲聰明，但她有著鑽牛角尖的傾向。恐怕那是優秀之人的宿命吧。

「對了！克萊伊醬，假如你沒得到寶具的話⋯⋯」

緊緊地抱住我的右臂，緊貼著我的莉茲自信滿滿地笑起來。

「我會從那個臭小鬼那裡偷來給你」

「⋯⋯⋯⋯對方是貴族喲？」

不對，就算不是貴族，偷盜也是不行的。
畢竟『盜賊』可不是那種角色。偷別人的東西就是犯罪。

「誒？那又怎樣啦？不要緊的，安於和平的騎士團無論有多少人，我都不會輸！」

「姐姐，你如果做了那種事，克萊伊桑會被懷疑的吧！要做的話⋯⋯是呢，裝成強盜之類的」

「別說了」

別說了。你們不懂克制嗎？
雖然應該只是玩笑，但如果被周圍的人聽到，可能會被他們當真不是嗎。

澤布魯迪亞拍賣會的會場是位於帝都中心部的白亞劇場。
是一個平時用於音樂會或看戲的場所。在大理石建造的精緻建築裡，年齡、性別和裝扮各異的人群正在排隊。

這裡面有多少人的目的是拍下拍賣品呢。究竟有多少人會與我們競爭呢。事到如今，我只希望拍賣會能愉快地結束。

入口按貴族、獵人、其他來劃分。貴族自不用說，而獵人之所以有專用入口，是因為把獵人和一般人放在一起肯定會引起問題。
澤布魯迪亞拍賣會的入場費是十萬吉爾。人最多且最多彩的是獵人的入口。

首先，聚集的獵人的裝扮就不同。為什麼要穿著全身甲來拍賣會啊。其次，相貌也各異，不知是誤會了什麼，帶著武器來的人也有。
因為參加拍賣會的都是擁有相應等級與金錢的獵人，所以會有幾張熟悉的面孔。

在這之中，我注意到了一個認識的集團。
有著衝天似的火紅頭髮的少年。黑褐色頭髮，神色嚴厲的壯年男性。棕色頭髮的女盜賊以及，摟著我右臂的傢伙的弟子。暗鍋成員久違地聚集一堂。
雖然周圍還有一些不認識的人，但我再怎麼說也不會看錯緹諾的臉。

猶豫著是向吉爾伯特少年搭話呢，還是向格雷格大人或者露達搭話，最終我選擇了向緹諾搭話。

「這不是緹諾麼。你們也是來競買？」

「！Master！早上好」

暗鍋成員看到我，像是心情不舒暢一樣，表情有一絲扭曲。
以一起組過隊為契機，開始共同行動了嗎？
不管怎麼樣，緹諾也交到了朋友比什麼都好。

承受著莉茲炯炯有神的目光，吉爾伯特少年和格雷格大人萎縮了。

「我是為看Master的英姿而來的。因為他們正打算來拍賣會，所以我就想順便一起」

「緹諾你，面對《千變萬化》時，性格就會變呢」

對於低聲嘀咕的吉爾伯特少年，緹諾投去了輕蔑的眼神。
雖然不是我有意為之，但在這次的拍賣會，我是旋渦中的人。吉爾伯特少年旁邊的男人饒有興致地看著我。能聽到竊竊私語聲。我真是如坐針氈。

「先不管能不能看到我的英姿，反正要來的話，一起來就好了⋯⋯」

「⋯⋯⋯⋯那個⋯⋯因為沒有邀請我⋯⋯」

「⋯⋯」

抱歉。真的抱歉。明明你連錢都借給我了，真的很抱歉。我完全沒有注意到。
可是，如果讓我辯解的話⋯⋯對了！大概比起和我一起來，我認為和露達她們一起來更好，不會惹人注目。我甚至希望和你交換位置。

露達用責備似的眼神看著我。她應該是知道我向緹諾借了錢吧。

「啊⋯⋯唔～嗯⋯⋯那個⋯⋯⋯⋯」

緹諾一直仰視著我。
我該怎麼跟她說呢。雖然現在也可以邀請她，但她已經有約在先了，而且待在莉茲和西特莉附近的話，她也會坐立難安吧。

就在此時，我想出了一個好主意。

「⋯⋯⋯⋯緹諾，如果你願意的話──要不要作為我的代理人參加競拍？」

「⋯⋯⋯⋯誒？代理人⋯⋯？」

澤布魯迪亞拍賣會有一個代理人制度。

正如其名，是一個派人代替自己參加競拍的制度。

雖然我們也會去競拍的場地，但不會出聲，而是發送獨特的暗號讓緹諾代為行事。
這主要是想隱瞞身份信息的中標者使用的制度，而我盯上『轉換的人面』是眾所周知的事，所以沒有什麼意義，但應該能對我享受拍賣會有些幫助吧。

我的提議令緹諾驚訝不已。西特莉眯起眼睛，微微點頭。

「⋯⋯原來如此⋯⋯不錯呢。雖然不知道大小姐會怎麼判斷，但這可以讓她混亂⋯⋯也可能沒有效果。不過沒關係嗎？克萊伊桑，你不是想親自拍下嗎？」

我確實很喜歡拍賣會。
參與白熱化的競拍，拍下想要之物時的舒暢感非常美妙，但我這次理應讓出來吧。

「我已經在去年和前年參與過好幾次啦。而且今年又是一團糟⋯⋯喂，莉茲。別擺出眼紅的樣子」

她也許是想當代理人吧，我訓誡了躍躍欲試的莉茲。
不管怎麼說都太沒大人樣了。

莉茲拖長了聲音回應我後，便瞪向緹諾。

「⋯⋯⋯⋯好～。嘖，緹諾，一定要贏喲」

「好，好的！請交給我吧，Master，姐姐大人！我一定會贏得那件寶具的！」

緹諾用力握住拳頭。
雖然讓她一定要贏，但因為資金有限，所以就算輸了也不是緹諾的錯。

一輛馬車駛近貴族用的入口。馬車上刻著眼熟的格拉迪斯家的紋章，穿著純白色禮服的女孩走下馬車，緊隨其後的是一位穿著管家服的年邁男人。
艾克蕾爾小姐張望起四周，在看到我後，就用不像是小我一輪的人會有的眼神瞪向這邊。

她在交涉的最後所露出的茫然表情，沒有絲毫殘留。看來是籌到超過兩億的錢了。

西特莉不露聲色，緊緊握住我的手。從側面看去，她依舊保持著笑容。
可是我看得出，她的表情深處藏著不安。

這是⋯⋯輸了嗎？