在與葉隱櫻相遇後。千鳥跑到了化妝室，一點點的拭干眼角的淚水。

「──啊啊，太好了。」

千鳥用參雜著安心的聲音，獨自喃喃道。

──千鳥對於葉隱櫻，一直有種無法言喻的罪惡感和惶惶不安的恐懼。

雖然有著在箱根時受到幫助的感激之情，但是感受到的恐懼感卻遠遠在其之上。而這一定是因為千鳥將她和『櫻姐姐』重疊在了一起吧。
芽吹提議DNA鑑定時，千鳥最先感受到的也是恐懼。被人懷疑與鶇的血緣關係，這已經不是第一次了。除了芽吹以外，千鳥也常常被天真無邪的友人和無心為之的大人指摘出兩人的『不相似』

而每次，千鳥都會因此感受到一種失去立足之地般的苦痛。
唯一殘留在身邊，作為追溯過去的線索的那張照片中，也只有「櫻姐姐」和鶇映入其中，仿彿就像千鳥並不是「家人」的證明一樣。而這個事實，也猶如巨石一般，重重壓在了千鳥的心頭。
記憶中的「櫻姐姐」，總是溫柔的微笑著。但是，為何呢？曾經那沁人心脾的笑容，如今卻讓人不寒而慄。

──這一定是千鳥那無法拭去的自我厭惡所致吧。

鶇之所以會盲目仰慕姐姐（千鳥），或許就是因為他在無意識中將自己真正的姐姐和千鳥重疊在了一起吧。這種毫無根據的不安，不斷的在心頭若隱若現。
沒有過去記憶的千鳥，現在唯一的家人──只有鶇了。如果千鳥和鶇不是姐弟的話，那麼「七瀨千鳥」這個存在又究竟要依靠什麼作為自己活下去的精神的寄托呢？每當這麼想的時候，身體變化不自覺的顫抖起來。
與鶇一起度過的每一個平凡的日常，總是讓人感到心滿意足，但是往往在不經意間，又會從中感到令人窒息的不安感。
在升入高中後，這種不安不但沒有消失，反倒像是火焰般，愈演愈烈。
而就在這時，那個魔法少女──葉隱櫻出現了。
與鶇，不，與「櫻姐姐」十分相似的那個魔法少女，自活動開始，轉眼間便升上了Ｃ級，並且在年末的拉冬戰的奇跡生還，一戰成名。
那一天，當千鳥在巴士上聽到葉隱櫻要與魔獸戰鬥的瞬間，就有一種被過去追上了感覺。這種感覺就像是在說，已經無處可逃了一樣。
雖然不知道她是否與「櫻姐姐」是同一人物，但是千鳥並不認為如此相似的人物會與她沒有關係。

──如果她是鶇的親人的話，她又會如何看待替換了「櫻姐姐」位置的千鳥呢？各種思緒匯入其中後，千鳥愈發不想與葉隱產生瓜葛了。

但是，在這次的相遇後，葉隱櫻的表面上──雖然並不知道她內心是怎麼想的，但是她確實還沒有與鶇接觸。或許正如她所說的，她確實是一位與鶇不相干的人物吧。即便她與鶇有所聯繫，如今特意表明自己不知情，也可以認為她是不想牽連其中。

──所以，在千鳥聽到她回答的那一刻，發自肺腑的鬆了一口氣。

只有葉隱櫻沒有和鶇接觸，鶇一定是無法回憶起「櫻姐姐」的事情。⋯⋯如果鶇恢復記憶的話，恐怕千鳥和鶇將不復以往了吧。
幸運的是鶇似乎對葉隱櫻並沒有什麼興趣，即使在電視上看到她的報道，也並沒有什麼特別在意的反應。倒不如說，因為與她的姿容相似，常常會被卷入不必要的麻煩中，所有對其有些避之若浼。
與擔心血緣關係的千鳥相反，他對兩人的關係沒有絲毫顧慮。而這樣的千鳥又究竟是如何得到救贖的，他一定還不知道吧。

──是不是就是因為這樣呢？千鳥察覺到了自己對鶇，抱有一些超越了家人的情感。

他不經意間的動作、歡笑時的特徵。生氣時的神情，失落時的聲音。這一切的一切，總會讓千鳥心動不已。

不過，講這稱為「戀」未免過於膚淺，但是，將其喚為「愛」又會過於沉重。

千鳥今後也一定不願鬆開鶇的手吧，即使被厭惡、被憎恨，也想要在他的身邊。這樣的「固執」，絕對不是「戀人」能夠做到的吧。
所以，千鳥為自己心意加上了掩飾。這份「愛」，絕非戀慕而應是親情才可以。
無論血脈是否相連，只要這麼想的話，千鳥的心靈就不會因此而受傷。
姐弟的話──作為，姐弟的話，就可以永遠和鶇在一起了。而且，鶇是一個溫柔的人，所以他一定不會拒絕「姐姐」的請求的。

──就像是，用言靈束縛起來一樣。

「⋯⋯我，真是太差勁了。」

臉色浮現出苦笑，千鳥喃喃道。對我著如此醜陋的內心，可愛的弟弟卻全然不知。

「──但是，這就是姐姐你選擇的道路吧。」
「小白⋯⋯」

不知何時，坐在千鳥肩上的白兔──千鳥的契約神用堅定的口吻說道。

「千鳥。你那時宣誓『無論發生什麼事情，你都會是你弟弟的伙伴』。那麼，就將其貫徹到底吧。畢竟，你是那孩子的姐姐吶。」
「嗯，是啊⋯⋯」

然後千鳥在稍微冷卻了一會變紅的眼睛後，便返回了自己所屬的部門。
休息時間雖然早早就結束了，但同僚在看到千鳥吶哭腫的眼睛後可能就心領神會了。在這個行業，經常會有人戰死沙場，而為此掩飾哭顏的人是比想像中還要多的。眾人也是因此而誤解了千鳥哭泣的理由了吧。
在規定的時間裡順利結束了今天的工作，回家後，千鳥便看到了正在準備晚飯的鶇。他正穿著黑色圍裙，一邊哼著歌一邊烹飪著料理。

「你回來啦。晚飯馬上就好，稍微等一下吧。」
「嗯，謝謝。這個味道，是在炸什麼嗎？」
「對，是鱈魚苗和沙鮻的天羅婦。主食的話，因為有一些竹筍，所以就做成竹筍（米）飯了。主食會以油炸食物為主，沒問題嗎？」（譯者：沙鮻，「キス」，與『Kiss』同音。不知道是什麼的話，可以看看《皿三昧》的第三話。『ご飯はタケノコがあったから炊き込みご飯』，簡單地說就是在蒸米飯前，將處理好的竹筍切片和淘好的米飯一起蒸。是日本那邊挺常見的一種烹飪方法，很多動畫中也有描述。）

鶇一邊回頭看來，一邊擔心的說道。可能是因為最近千鳥的食量比較小，所以感到擔心了吧。

「沒問題。⋯⋯不過，這次真是下功夫了呢。全部都是當季的食材吶。」
「唔──嗯，可能是最近突然覺醒了對『食』的樂趣吧。為了追求美食的樂趣，我還打起工了呢。」
「打工也是可以理解，但是還不要太勉強自己了。雖然給我買土特產我也是很開心啦，不過適度就可以了。」

鶇從二月開始好像開始打工了，理由則是因為千鳥在政府工作，而只有自己一個人待家裡多少還是會感到很難受。當天一提到政府的工作，千鳥也就沒法多說什麼了。
周末，有時也會為了確保移動路線，被政府指派至全國各地，所以與鶇在一起的時間也隨之驟減。或許，鶇是因此感到了寂寞也說不定。
在問他打工的地點時，他常常以「太羞恥了」不願告訴千鳥，所以千鳥猜測他應該是在飲食類的地方工作。
而且，如果要說有什麼問題的話，大概就是鶇使用資金的事情了。鶇常常會周末出門，而每次回來時都會帶回昂貴的土特產。一開始千鳥還覺得他只是因為初次打工，所以有些闊綽罷了，但是數次之後千鳥還是對此感到了擔心。
雖然還沒有動用監護人夜鶴提供的生活費，但是考慮到今後他的金錢觀，鶇這樣大手大腳的習慣可不是什麼好事。
跟他提及後，他也只會「我會注意的」敷衍，像這樣子他肯定不會認真去考慮的吧。如果下次他再買昂貴的特產的話，到時候一定得好好說說他。

⋯⋯到時候也拜託芽吹前輩來幫我吧。千鳥一邊想著這些事情，一邊坐在了沙發上。今晚白不在，看樣子是有什麼事情吧。

「說起來，千鳥。黃金周你有什麼安排嗎？」

鶇一邊將料理盛入餐盤，一邊提起了這件事。

「和往年一樣，我會去參加劍道部的合宿。⋯⋯雖然不能參加大會，但至少可以幫她們訓練或分擔一些事情。」

成為魔法少女後，便失去了大會的出場資格，所以千鳥一有時間便會前往劍道部協助訓練。因為在千鳥退部的時候，被其他部員強烈的懇請了。而千鳥也覺得這不失為一種檢測身體能力的方法，所以也就不覺得這是一種負擔。

「這樣啊，因為黃金周的周六約好了和朋友一起去玩，可以的話也向邀請你一起。不過你有事情的話，那就沒辦法了。」
「朋友？是班裡的人吧。鶇和他們的關係還怎麼吶。我在走廊遇到他們的時候，他們也常常會說到你呢。」
「⋯⋯不是的吧，那群傢伙只是想要和千鳥搭話吧。而且這次和我一起去的並不是同學哦。說起來，行貴好像要在連休和有錢的小姐姐去坐游輪。」

對於鶇的回答，千鳥不由得露出了苦笑。看樣子鶇的好友，天吏還是老樣子吶。

「那麼，究竟是誰呢？你和我都認識的人，也就只有芽吹前輩了吧。」
「是鈴城和壬生啦。千鳥你不是也和她們交換了聯絡方式嗎？」

──吧嗒。

在聽到鶇說辭的瞬間，千鳥把遞來的碗掉了下去。碗裡的米飯撒在了桌子上。

「哇，沒事吧？好像沒有碎，不過得重新盛了吶。」

因為還有很多，所以不用擔心，鶇微笑著說道。千鳥用著困惑的神情望著鶇，開口道

「確實是和她們交換了聯繫方式，但是我和她們最多也就只有過社交層面的問候而已。而且，對我來說她們兩人就像是上司一樣⋯⋯⋯倒不如說，我對鶇你能和她們的關係好到可以一起出去玩的程度，感到了不可思議呢⋯⋯」

千鳥一邊這麼說著，一邊思索起鶇和鈴城與壬生關係變好的原因。

──鶇，他本人可能沒有自覺，但是他身上確實有著能夠吸引特殊人群的某種事物。

以問題兒童的同班同學為首，放蕩不羈的天吏，還有被譽為學院首屈一指的麒麟兒芽吹──每個人，都對鶇青睞有加。
天選之子的六華兩人，大概也是被鶇的這種特異性所吸引了吧。

「但是」，如此想到的千鳥看著眼前的鶇

⋯⋯這個樣子，似乎並沒有什麼戀愛的要素在裡面吶。

兩人談話的時候，鶇似乎也沒有什麼特殊的反應。雖然對此感到了開心，但是卻沒有看到戀慕的傾向。好像真的是打算作為「普通朋友」，與她們相見的。

「這次就不要在意我了。好好享受吧」

千鳥笑著如此說道後，鶇便微笑著開口道

「嗯。⋯⋯到夏天，假期增加了的話，下次一起去什麼地方旅行吧。最近，一直沒有什麼機會和千鳥一起出去。」
「是啊⋯⋯⋯我的工資也要入賬了，下次就讓我們好好放鬆下吧。」

鶇的關心，讓千鳥的心靈也感受到了溫暖。

──啊～啊，真希望這平穩的幸福能夠永遠持續下去。千鳥一邊考慮著這些事情，一邊將手伸向了這準備好了的溫暖料理。