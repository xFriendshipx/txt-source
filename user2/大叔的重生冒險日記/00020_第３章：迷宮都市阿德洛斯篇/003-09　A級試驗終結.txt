這樣男子氣概戰士的人化龍，有誰受得了啊！拿美女出來！
他會龍息。也會飛。因為那是真的龍。

他不知怎麼就走過來，問我。
「你是什麼人。從沒聽說過有會吐息的人。」
「並不是沒有啊。雖然很少。」

「沒想到會有比我還強的吐息。認輸。我還修行不足。」
說完，他裁判向宣布投降，舉起一隻手走了出去。

啊？到底是怎麼回事？目瞪口呆，我呆呆地目送著那傢伙。

「勝利者阿爾馮斯。」
不太熱烈。可惡。總比沉默好嗎？我快要哭了。

當我看著他時，他就和妻子與孩子笑著聊天。然後，抱起孩子開心地走了。夫人是人！而且，非常漂亮。孩子也超可愛。

孩子是人龍混血。是第一次見到的種族。上帝保佑你。希望那個孩子將來不要被歧視。

那傢伙，是因為與人類結婚才人化住在城市裡嗎？總覺得，又湧來了失敗感。我啊……。
不行，不行。還有剩下的比賽。

還有兩場比賽。下一場是準決賽。接下來的對手是穿著奇怪的鎧甲的男人。由內而上的衝動。


　　　　　『警戒』

簡潔的兩個字，伴隨著難以置信的壓力在腦海中流淌。從來沒有這樣啊！【那傢伙】的強烈警告。而且還是二重鉤括弧！
特意用【語言】來警告，真的那麼糟糕嗎！？

什麼？剛才的龍男也沒有反應。這究竟是？
一邊警戒一邊試著一開始就打出【耀斑】。應該向那傢伙全力前進的【耀斑】突然消失了！？

是消除魔法嗎？沒過多久，他就以最快的速度使出了身體強化最大、最大強化的秘銀劍。一點傷痕都沒有。這讓我大吃一驚。

「沒用。不管你怎麼強大，也無法給予穿上這鎧甲的我傷害。這是一種詛咒。魔法效果無效、物理攻擊無效。毒也被看作物理攻擊。你打不倒我的。」

「詛咒的效果是？」
「這是一生都脫不掉的？」
我絕對不喜歡那樣的鎧甲。

「可以問一個問題嗎？」
「什麼？」
「那個，上廁所呢？」
「……嘛，也有生活魔法這種東西。」

用流水和魔法處理嗎？像太空飛行員那樣的傢伙。比用尿布好嗎？再說一遍，如果是我絕對不要。

「好了，談話結束了。再見了！」

他拔出了秘銀大劍，動作極其粗魯，給人以鍛鍊積累了鍛鍊的強者的印象。只用劍戰鬥的話，就算拼了1兆次，也贏不了這傢伙！

糟糕，這傢伙的技能也很不錯。一陣冷汗從我的脊樑上飛奔而下。

好！要不要試一下？在他的周圍，稍微圍上【盾】，試著抽出裡面的空氣。瞬間進入道具箱。這樣的話，那個鎧甲也會無效吧。

不，他倒下了。真的假的！

避難的裁判趕來宣佈。
「勝利者阿爾馮斯。」

如果，他有現代人的知識的話，這樣也能應付得了吧。用風魔法製造空氣就可以了。

但是，那傢伙對呼吸結構沒有詳細的瞭解。一瞬間被奪走了空氣，無法應對。

【治療】之後蘇醒了。但是，是什麼呢。雖然警告很激烈，但還是順利地解決了。雖然有點不能接受，不過也有這樣的事。

「我輸了嗎……。你真厲害。一定要拿冠軍。」
只留下了短暫的應援，那傢伙走了。是相當心情好的傢伙。又是強敵。

終於決賽了。想到他是個什麼樣的傢伙，原來是個魔法使。而且，這傢伙一點也不帥。也不會胡亂用魔法挑釁。然後，他接近了我。

「我有個建議。」
哎呀？

「我和你互相施展魔法的話，恐怕這個會場會不復存在。」
嗯嗯。

「我們互相用【盾】圍著保護會場。不然兩個人將會……」

我知道了。大眼珠必死。我本來打算自己做的，那傢伙是個很會照顧人的男人。

「瞭解。我也被公會長這麼說。」

「那就去吧。我期待著你的魔法。」
那麼，回答那個期待吧。【盾】，【魔法盾】，【屏障】。那個已經足夠了。那傢伙也一個勁兒地幹。

「那麼開始吧。隨時都可以哦。」
「瞭解！」

【魔道鎧】的威力過高，差點把對手殺死了。作為緊急情況的保險處理。多虧了你，我才落後於那個暗殺者的老爺爺。

到現在為止封印著，不過，這裡當然是要使用的場景。但是，速度被封印了。站在那裡的魔法互相攻擊。

對方已經開打了。道具箱中儲存著「魔法原件」。也準備了很多複製品。從那裡向自己用防禦魔法，對手發射攻擊魔法。

向對方表示敬意，封印可笑的攻擊魔道具和物理武器的使用。互相享受了純粹的魔法的互相射擊。

那已經很華麗了。宛如地面上施放的煙花大會。會場裡的人都目不轉睛地看著它。

火焰、爆炸、雷、冰、土、風等各種魔法不斷地襲擊。如果有和我有著相同看法的人，一定會很開心吧。

大概持續了一個小時吧。不久，那個嚴肅地迎接了結束。是他的砲擊停止了。我也停止了射擊。然後那傢伙轉過身來，靜靜地退出了會場。

「勝者阿爾馮斯。」
我贏了。感謝魔法師的他。與A級的決賽相稱的內容。

至少，他的精神最低也是A級。如果說在戰場上互相交火，我獲勝了，我會容許成為了俘虜的他攜帶配劍，然後拍個紀念照。

原以為最後什麼都沒有，沒想到冠軍卻被國王授予了A級資格。公會雖然是橫跨國家間的獨立組織，但主辦國照顧了。
我被叫到名字，向前走去，彎著膝蓋低下了頭。

「這是冠軍大典。如果想在我國做官，隨時歡迎。那時候也授予子爵位吧。從我國授予什麼獎賞吧。什麼好？」
國王陛下說了。不是名譽子爵位，是子爵位嗎？那個是又給我買的。

「這樣的話，我很抱歉地告訴您。我先拿到了獨行屠龍者的稱號。請以那個功績推舉成為S級。
因而，想請王家奉獻龍。怎麼樣呢？」

於是，在會場裡放出了50M的龍的屍體。會場一片嘈雜。
「嗯。龍收到了。認為是充分的功績。阿爾伯特洛斯王家25代國王，將A級冒險者阿爾馮斯推舉為S級。」

公會長說，這樣就正式決定了。與公會長私下通過談話，應該沒有問題。之後是公會的問題，公會長用許可權升到S級。

因為有了這樣的形式，可笑的貴族們檯面上也不能對我出手了。

我說出來了，他們到現在為止的所有壞事，能全部都公開了。當然，因為對方知道我想要這樣，所以他們不能公開出手。

從背面來的東西，毫不留情地毀壞。終於能如願以償地整頓體制。
好了，開路邊攤的時間到了！
（譯註：屋台，路邊攤。）